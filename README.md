﻿BezierMaker
--
通过de Casteljau算法绘制贝塞尔曲线，并计算它的切线，实现1-7阶贝塞尔曲线的形成动画。
德卡斯特里奥算法可以计算出Bezier曲线上的一个点，进而绘制出Bezier曲线。

[Bezier曲线的原理](https://blog.csdn.net/venshine/article/details/51758841)

想深入了解德卡斯特里奥算法的同学可以参考这篇文章[《德卡斯特里奥算法——找到Bezier曲线上的一个点》](http://blog.csdn.net/venshine/article/details/51750906)。  

Features
--
* 支持增加和删除控制点
* 支持1阶到7阶贝塞尔曲线，限于屏幕大小，理论上可以支持N阶贝塞尔曲线
* 支持自由移动控制点
* 支持显示贝塞尔曲线形成过程的切线
* 支持循环显示贝塞尔曲线的形成动画
* 支持贝塞尔曲线显示速率
* 支持显示控制点坐标
* 支持设置贝塞尔曲线阶数


ScreenShot
--
<img src="https://gitee.com/HarmonyOS-tpc/BezierMaker/raw/master/screenshot/screenshot5.gif" width="33%">  
<img src="https://gitee.com/HarmonyOS-tpc/BezierMaker/raw/master/screenshot/screenshot2.gif" width="33%">    
<img src="https://gitee.com/HarmonyOS-tpc/BezierMaker/raw/master/screenshot/screenshot6.gif" width="33%">
<img src="https://gitee.com/HarmonyOS-tpc/BezierMaker/raw/master/screenshot/screenshot4.gif" width="33%"> 
<img src="https://gitee.com/HarmonyOS-tpc/BezierMaker/raw/master/screenshot/screenshot3.gif" width="33%">  
<img src="https://gitee.com/HarmonyOS-tpc/BezierMaker/raw/master/screenshot/screenshot1.gif" width="33%"> 


How to import into your project
--

方式一：
通过library生成har包，添加har包到libs文件夹内
在entry的gradle内添加如下代码
```gradle
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```

方式二：
```gradle
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:BezierMaker:1.0.5'
```


Demo
--

##### Java:
```Java
 public class MainAbility extends Ability {

        BezierView mBezierView;

        @Override
         protected void onStart(Intent intent) {
           super.onStart(intent);
           setUIContent(ResourceTable.Layout_ability_main);
           
           mBezierView = (BezierView) findComponentById(ResourceTable.Id_bezier);
        }

        public void start(Component component) {
            mBezierView.start();
        }

        public void stop(Component component) {
            mBezierView.stop();
        }

        public void add(Component component) {
            mBezierView.addPoint();
        }

        public void del(Component component) {
            mBezierView.delPoint();
        }

    }
```

##### Methods:
| method 方法          | description 描述 |
|:---				 |:---|
| void **start**()  	     | 开始贝塞尔曲线（required） |
| void **stop**()	     | 停止贝塞尔曲线（optional） |
| boolean **addPoint**() 	     | 增加控制点（optional） |
| boolean **delPoint**() 	     | 删除控制点（optional） |
| int **getOrder**() 	     | 获取贝塞尔曲线阶数（optional） |
| void **setRate**(int rate) 	     | 设置移动速率（optional） |
| void **setTangent**(boolean tangent)  	     | 设置是否显示切线（optional） |
| void **setLoop**(boolean loop)  	     | 设置是否循环（optional） |
| void **setOrder**(int order)  	     | 设置贝塞尔曲线阶数（optional） |


License
--
    Copyright (C) 2016 venshine.cn@gmail.com

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.