/*
 * Copyright (C) 2016 venshine.cn@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wx.beziermaker.demo;

import com.wx.beziermaker.BezierView;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Slider;
import ohos.agp.components.element.VectorElement;
import ohos.agp.window.dialog.ToastDialog;

import java.util.Locale;

/**
 * Demo
 *
 * @author venshine
 */
public class MainAbility extends Ability implements Component.ClickedListener {
    private BezierView mBezierView;
    private Text mTextView;
    private Image mLoopIv;
    private Image mTangentIv;
    private boolean mLoop = false;
    private boolean mTangent = true;
    private VectorElement mDisableElt;
    private VectorElement mEnableElt;


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        mBezierView = (BezierView) findComponentById(ResourceTable.Id_bezier);
        mTextView = (Text) findComponentById(ResourceTable.Id_title);
        mTextView.setText(String.format(Locale.ROOT, "%s阶贝塞尔曲线", mBezierView.getOrderStr()));
        Button add = (Button) findComponentById(ResourceTable.Id_add);
        Button del = (Button) findComponentById(ResourceTable.Id_del);
        Button start = (Button) findComponentById(ResourceTable.Id_start);
        Button stop = (Button) findComponentById(ResourceTable.Id_stop);
        add.setClickedListener(this);
        del.setClickedListener(this);
        start.setClickedListener(this);
        stop.setClickedListener(this);

        DirectionalLayout loopDl = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_switch_loop);
        DirectionalLayout tangentDl = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_switch_tangent);
        loopDl.setClickedListener(this);
        tangentDl.setClickedListener(this);

        mLoopIv = (Image) findComponentById(ResourceTable.Id_iv_loop);
        mTangentIv = (Image) findComponentById(ResourceTable.Id_iv_tangent);
        mDisableElt = new VectorElement(this, ResourceTable.Graphic_icon_switch_disable);
        mEnableElt = new VectorElement(this, ResourceTable.Graphic_icon_switch_enable);

        Slider slider = (Slider) findComponentById(ResourceTable.Id_seekbar);
        slider.setValueChangedListener(
                new Slider.ValueChangedListener() {
                    @Override
                    public void onProgressUpdated(Slider slider, int progress, boolean b) {
                        int value = progress;
                        if (progress == 0) {
                            value = 1;
                        }
                        mBezierView.setRate(value * 2);
                    }

                    @Override
                    public void onTouchStart(Slider slider) {
                    }

                    @Override
                    public void onTouchEnd(Slider slider) {
                    }
                }
        );
    }


    @Override
    protected void onActive() {
        super.onActive();
    }


    private void start(Component component) {
        mBezierView.start();
    }

    private void stop(Component component) {
        mBezierView.stop();
    }

    private void add(Component component) {
        if (mBezierView.addPoint()) {
            mTextView.setText(String.format(Locale.ROOT, "%s阶贝塞尔曲线", mBezierView.getOrderStr()));

        } else {
            ToastDialog toastDialog = new ToastDialog(this);
            toastDialog.setText("Add point failed.");
            toastDialog.show();
        }
    }

    private void del(Component component) {
        if (mBezierView.delPoint()) {
            mTextView.setText(String.format(Locale.ROOT, "%s阶贝塞尔曲线", mBezierView.getOrderStr()));
        } else {
            ToastDialog toastDialog = new ToastDialog(this);
            toastDialog.setText("Delete point failed.");
            toastDialog.show();
        }
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_add:
                add(component);
                break;
            case ResourceTable.Id_del:
                del(component);
                break;
            case ResourceTable.Id_start:
                start(component);
                break;
            case ResourceTable.Id_stop:
                stop(component);
                break;
            case ResourceTable.Id_dl_switch_loop:
                mLoop = !mLoop;
                if (mLoop) {
                    mLoopIv.setImageElement(mEnableElt);
                } else {
                    mLoopIv.setImageElement(mDisableElt);
                }
                mBezierView.setLoop(mLoop);
                break;
            case ResourceTable.Id_dl_switch_tangent:
                mTangent = !mTangent;
                if (mTangent) {
                    mTangentIv.setImageElement(mEnableElt);
                } else {
                    mTangentIv.setImageElement(mDisableElt);
                }
                mBezierView.setTangent(mTangent);
                break;
            default:
                break;
        }
    }
}
