/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wx.beziermaker;

import ohos.agp.components.Component;
import ohos.multimodalinput.event.TouchEvent;

/**
 * 获取坐标工具类
 */
public class EventUtil {
    /**
     * 相对于屏幕坐标
     *
     * @param touchEvent 触摸事件
     * @return 返回相对于屏幕坐标x
     */
    public static float getRawX(TouchEvent touchEvent) {
        return touchEvent.getPointerScreenPosition(0).getX();
    }

    /**
     * 对于屏幕坐标
     *
     * @param touchEvent 触摸事件
     * @return 返回相对于屏幕坐标y
     */
    public static float getRawY(TouchEvent touchEvent) {
        return touchEvent.getPointerScreenPosition(0).getY();
    }

    /**
     * 相对于组件
     *
     * @param component  组件
     * @param touchEvent 触摸事件
     * @return 返回相对于组件坐标x
     */
    public static float getXInComponent(Component component, TouchEvent touchEvent) {
        int[] locationOnScreen = component.getLocationOnScreen();
        return getRawX(touchEvent) - locationOnScreen[0];
    }

    /**
     * 相对于组件
     *
     * @param component  组件
     * @param touchEvent 触摸事件
     * @return 返回相对于组件坐标y
     */
    public static float getYIntComponent(Component component, TouchEvent touchEvent) {
        int[] locationOnScreen = component.getLocationOnScreen();
        return getRawY(touchEvent) - locationOnScreen[1];
    }
}
